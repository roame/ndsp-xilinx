# This is a stand-alone xilinx nix environment suitable for development of the driver.
# A different script will show how to intergrate the xilinx with an existing artiq environment. 

{
    description = "NDSP for Xilinx RFSoC 4x2";

    inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-22.05;
    inputs.sipyco.url = github:m-labs/sipyco;
    inputs.sipyco.inputs.nixpkgs.follows = "nixpkgs";
    
    outputs = { self, nixpkgs, sipyco }:
        let 
        pkgs = nixpkgs.legacyPackages.x86_64-linux; 
        asyncserial = pkgs.python3Packages.buildPythonPackage rec {
            pname = "asyncserial";
            version = "0.1";
            src = pkgs.fetchFromGitHub {
                owner = "m-labs";
                repo = "asyncserial";
                rev = "446559fec892a556876b17d17f182ae9647d5952";
                sha256 = "1y63ibfvwcph5zvvbrnbvqa7al97g8pxw5dwqgv33cbr3s16ck2q";
            };
            propagatedBuildInputs = with pkgs.python3Packages; [ pyserial ];
        };
    in {
        defaultPackage.x86_64-linux = pkgs.python3Packages.buildPythonApplication ({
                name = "aqctl_valon";
                version = "1.0";
                src = ./.;
                propagatedBuildInputs = [ sipyco.packages.x86_64-linux.sipyco asyncserial ];
        });
    };
}
