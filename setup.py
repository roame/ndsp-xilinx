from setuptools import setup, find_packages

setup(
    name="xilinx",
    install_requires=["sipyco", "asyncserial"],
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "aqctl_xilinx = xilinx.aqctl_xilinx:main",
        ],
    },
)