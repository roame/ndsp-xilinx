from xilinx.driver import RampGenerator
from sipyco.pc_rpc import simple_server_loop
from sipyco import common_args

class NetInterface:
    def __init__(self):
        self.gen = RampGenerator(f=50e6,a=0.0)
        
    def gen_saw(self, vals):
        # vals must be a list of the form [freq (MHz), amp (ul)]
        if vals[0] < 0:
            vals[0] = 0
        elif vals[0] > 1000:
            vals[0] = 1000
        
        if vals[1] > 1:
            vals[1] = 1
        elif vals[1] < 0:
            vals[1] = 0
        
        self.gen.frequency = vals[0]*1e6  # Hz
        self.gen.amplitude = vals[1]
    
    def stop(self):
        # Simply sets the amplitude to zero
        self.gen.amplitude = 0.0

        
def get_argparser():
    parser = argparse.ArgumentParser(description="ARTIQ controller for the Xilinx RFSoC 4x2")
    common_args.simple_network_args(parser, 3254)
    parser.add_argument("-d", "--device", default=None, help="serial port or IP address.")
    common_args.verbosity_args(parser)
    return parser

def main():
    args = get_argparser().parse_args()
    common_args.init_logger_from_args(args)
    
    interface = NetInterface()
    try:
        simple_server_loop({"xilinx": interface}, common_args.bind_address_from_args(args), args.port)  # port 9091 typically used
    finally:
        interface.stop()

if __name__ == "__main__":
    main()